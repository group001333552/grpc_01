﻿using System;
using System.Runtime.Serialization;

namespace server
{
    [Serializable]
    internal class IOExecption : Exception
    {
        public IOExecption()
        {
        }

        public IOExecption(string message) : base(message)
        {
        }

        public IOExecption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IOExecption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}